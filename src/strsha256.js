async function strsha256 (input) {
	const inputArray = new TextEncoder().encode (input)
	const outputBuffer = await crypto.subtle.digest('SHA-256', inputArray)
	const outputArray = Array.from (new Uint8Array (outputBuffer))
	const outputHex = outputArray.map (b => b.toString (16).padStart (2, '0')).join('')
	return outputHex
}


function strrnd256 () {
	let array = new Uint32Array (8)
	window.crypto.getRandomValues (array)
	return Array.from (array).map (x => x.toString (16).padStart (8, '0')).join('')
}

